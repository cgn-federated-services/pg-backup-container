FROM docker.io/library/postgres:16.3-alpine3.19

RUN apk add --update-cache \
    gpg         \
    gpg-agent;  \
    rm -rf /var/cache/apk/*

COPY --from=docker.io/minio/mc:RELEASE.2024-02-24T01-33-20Z /usr/bin/mc /usr/bin/

COPY backup.sh /usr/bin/pg-backup

ENTRYPOINT [""]

CMD ["/usr/bin/pg-backup"]