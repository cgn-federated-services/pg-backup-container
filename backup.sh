#!/bin/bash

[ -z ${POSTGRES_DB+x} ] && echo "env POSTGRES_DB is unset, exiting" && exit 1
[ -z ${POSTGRES_USER+x} ] && echo "env POSTGRES_USER is unset, exiting" && exit 1
[ -z ${POSTGRES_PASSWORD+x} ] && echo "env POSTGRES_PASSWORD is unset, exiting" && exit 1
[ -z ${POSTGRES_HOST+x} ] && echo "env POSTGRES_HOST is unset, exiting" && exit 1
[ -z ${MC_HOST_S3+x} ] && echo "env MC_HOST_S3 is unset" && exit 1
[ -z ${MC_BUCKET+x} ] && echo "env MC_BUCKET is unset, exiting" && exit 1
[ -z ${MC_PATH+x} ] && echo "env MC_PATH is unset, exiting" && exit 1
[ -z ${GPG_PASSWORD+x} ] && echo "env GPG_PASSWORD is unset, exiting" && exit 1
[ -z ${BACKUP_SCHEDULE+x} ] && echo "env BACKUP_SCHEDULE is unset disabling schedule" && BACKUP_SCHEDULE=""

backup_db(){
    echo "Backing up Database"
    file_name=$(date "+db_${POSTGRES_DB}_%m%d%Y%H%M.sql.gz.gpg")
    echo "${POSTGRES_PASSWORD}" | \
        pg_dump -Fc -v -h "${POSTGRES_HOST}" -U "${POSTGRES_USER}" -d "${POSTGRES_DB}" | \
        gpg -c --batch --no-tty --cipher-algo AES-256 --passphrase "${GPG_PASSWORD}" -o - | \
        mc pipe "S3/${MC_BUCKET}/${MC_PATH}/${file_name}"
    echo ""
    echo "Backup successful"
}

[ "${STARTUP_BACKUP}" == "true" ] || [ "${BACKUP_SCHEDULE}" = "" ] && echo "STARTUP_BACKUP or no BACKUP_SCHEDULE set, starting backup now" && backup_db

if [ "${BACKUP_SCHEDULE}" == "" ]; then
    randome_delay=$((RANDOM % 1800))
    BACKUP_SCHEDULE=$((randome_delay+BACKUP_SCHEDULE))

    echo "Added randome delay of ${randome_delay}s, the final schedule is every ${BACKUP_SCHEDULE}s"

    while true
    do
        sleep $BACKUP_SCHEDULE
        backup_db
    done
fi